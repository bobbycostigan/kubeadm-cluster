# Kubeadm cluster

This playbook repo sets up a single control-plane kubernetes cluster on CentOS 7. This is not an alternative to kubespray, I'm only supporting a specific set of technologies.

* etcd
* Kubeadm/kubelet and so forth
* calico pod networking
* ingress-nginx

## Default vagrant environment

Copy the default environment to your own when you're ready to deploy. For example ``cp -r inventory/default inventory/myown``.

### This will run bootstrap.yml and install necessary container services

    $ vagrant up

#### If you're not running vagrant, run bootstrap.yml manually

    $ ansible-playbook -i inventory/default/hosts bootstrap.yml

### Configure your SSH access to the nodes.

    $ vagrant ssh-config

### This will init the master cluster node and prepare the workers to join.

    $ ansible-playbook -i inventory/default/hosts setup-cluster.yml

It should also print a token and cert hash value at the end.

### This will join all the workers to the cluster.

    $ ansible-playbook -i inventory/default/hosts join-cluster.yml -e 'token=abc.123' \
      -e 'cert_hash=sha256:xxx' \
      -e 'k8s_apiserver_advertise_address=192.168.122.xx'

You must set ``k8s_apiserver_advertise_address`` to the IP of your master node.

Wait a while now and monitor the following commands before you move on.

    $ kubectl get nodes --watch
    $ kubectl get pods --all-namespaces --watch

It might take several minutes before kubelet logs stop reporting networking errors, all calico pods are deployed and all nodes are Ready.

## Optional

### Deploy some essential system services in k8s like ingress-nginx

    $ ansible-playbook -i inventory/default/hosts system-services.yml

### Deploy an example user defined service based on Python and Flask in k8s

    $ ansible-playbook -i inventory/default/hosts user-services.yml

# Kubernetes Tutorials

Once you've deployed a cluster to vagrant you can start with [this Kubernetes tutorial](https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-intro/) and follow the steps in your own cluster.

Note that sometimes the interactive tutorial will tell you to call a pod service through kube-proxy using a command like this.

    $ curl "http://localhost:8001/api/v1/namespace/default/pod/$POD_NAME/proxy/"

And if that doesn't work because your pod is using port 8080 then you can simply add the port like this.

    $ curl "http://localhost:8001/api/v1/namespace/default/pod/$POD_NAME:8080/proxy/"

